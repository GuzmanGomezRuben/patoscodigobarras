import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect
} from 'react-router-dom';
import Registro from './componentes/registro/Registro';
import './App.css';
import Juego from './componentes/juego/Juego';
import AuthService from './services/AuthService';
import Loader from './componentes/loader/Loader';

class App extends Component {

  render() {
    return (<Router>
      <div className="router-content">
        <LoginRoute exact path="/" component={Registro} />
        <PrivateRoute path="/game" component={Juego} />
      </div>
    </Router>)
  }
}


const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      AuthService.isLogin() ? (
        <Component {...props} />
      ) : (
          <Redirect
            to={{
              pathname: "/",
              state: { from: props.location }
            }}
          />
        )
    }
  />
);

const LoginRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      !AuthService.isLogin() ? (
        <Component {...props} />
      ) : (
          <Redirect
            to={{
              pathname: "/game",
              state: { from: props.location }
            }}
          />
        )
    }
  />
);

export default App;
