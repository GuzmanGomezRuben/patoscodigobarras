const AuthService = {
    isLogin : function(){
        return !!localStorage.getItem('token');
    },
    login : function(token){
        localStorage.setItem('token', JSON.stringify(token));
    },
    logout: function(){
        localStorage.removeItem('token');
    },
    user: function(){
        let user = localStorage.getItem('token');
        return user ? JSON.parse(user) : null;
    }
}

export default AuthService;
