import Axios from 'axios';
import Config from '../componentes/Config';
import Helpers from '../componentes/Helpers';

const UserService = {
    register: function (user) {
        return Axios.post(Config.endpoints.players.register, user);
    },
    all: function () {
        return Axios.get(Config.endpoints.players.all);
    },
    update: function (data) {
        var url = Config.endpoints.players.update;

        return Axios.put(url, data);
    }
}

export default UserService;
