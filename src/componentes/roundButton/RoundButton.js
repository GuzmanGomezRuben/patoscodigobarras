import React, {Component} from 'react';
import './RoundButton.css'

class RoundButton extends Component{



    render(){
        return (<div className="inline" onClick={() => (!this.props.disabled && this.props.onClick())}>
            <div className={"round-button " + (this.props.disabled ? "disabled" : "")} >
                <div className="round-button-circle">
                    <span className="round-button">{this.props.name}</span>
                </div>
        </div></div>)
    }

}

export default RoundButton;
