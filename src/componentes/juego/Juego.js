import React, { Component } from 'react';
import Pato from '../Pato';
import Marcador from '../Marcador';
import RoundButton from '../roundButton/RoundButton';
import Pausa from '../Pausa';
import './Juego.css'
import AuthService from '../../services/AuthService';
import User from '../../services/UserService';

class Juego extends Component {


    constructor(props) {
        super(props);
        this.espaciosDisponibles = [0, 1, 2, 3, 4];
        this.maximoPatos = 10;
        this.setvelocidadDefault = 20;
        this.state = {
            disparosEnPatos: 0,
            patos: [],
            numeroDePatos: 1,
            pause: false,
            started: false,
            lastLine: null,
            lastSide: null
        };
        this.identificadorLeido = '';
        this.aumentarContador = this.aumentarContador.bind(this);
        this.start = this.start.bind(this);
        this.pause = this.pause.bind(this);
        this.stop = this.stop.bind(this);
        this.onLost = this.onLost.bind(this);
        this.logout = this.logout.bind(this);
        this.patos = {};

        window.addEventListener('keyup', function (key) {
            this.identificadorLeido += key.key;
            console.log(key);
            if (key.key === '.') {
                console.log(this.identificadorLeido);
                this.aumentarContador(this.identificadorLeido);
                this.identificadorLeido = '';
            }

        }.bind(this));
    }

    aumentarContador(id) {
        let patosNuevos = this.state.patos.slice();
        let pato = patosNuevos.find((pato) => pato.id === id);
        let indice = patosNuevos.indexOf(pato);

        delete this.patos[id];

        this.validaDificultad();

        if (indice === -1) {
            console.log(id, patosNuevos);
            console.error("No encontro el pato");
        }

        patosNuevos.splice(indice, 1);

        this.setState(prevState => ({
            disparosEnPatos: ++prevState.disparosEnPatos,
            patos: patosNuevos
        }));

    }

    validaDificultad() {
        var cantidad = this.state.disparosEnPatos;
        switch (cantidad) {
            case 5: this.velocidadDefault = 18; break;
            case 15: this.velocidadDefault = 13; break;
            case 20: this.velocidadDefault = 10; break;
            case 25: this.velocidadDefault = 5; break;
            case 30: this.velocidadDefault = 1; break;
        }

        this.clearIntervals();
        this.start();

    }



    onLost() {
        this.pause();
        let user = AuthService.user();
        console.log('Puntos:', {
            disparosEnPatos : this.state.disparosEnPatos,
            user: user.puntos
        })
        if(this.state.disparosEnPatos > user.puntos){
            User.update({
                nombre: user.nombre,
                puntos : this.state.disparosEnPatos
            });

        }
        
        alert('Perdiste');
    }

    start() {
        this.setState({ started: true });
        this.intervalId = window.setInterval(this.agregarNuevoPato.bind(this), 1000);
        this.moverPatosInterval = window.setInterval(this.moverPatos.bind(this), this.setvelocidadDefault);
    }

    stop() {
        this.clearIntervals();
        this.setState(prevState => ({
            disparosEnPatos: 0,
            patos: [],
            started: false,
            paused: false
        }));

    }

    clearIntervals() {
        if (this.intervalId) {
            window.clearInterval(this.intervalId);
        }

        if (this.moverPatosInterval) {
            window.clearInterval(this.moverPatosInterval);
        }
    }

    pause() {
        this.setState((prevState) => ({ paused: !prevState.paused }));

        this.clearIntervals();

        if (this.state.started && this.state.paused) {
            this.start();
        }
    }

    moverPatos() {
        for (let pato in this.patos) {
            if (this.patos.hasOwnProperty(pato) && this.patos[pato]) {
                this.patos[pato].setStyles();
            }
        }

    }

    agregarNuevoPato() {
        this.setState(prevState => {
            if (prevState.patos.length > this.maximoPatos || this.state.paused) {
                return null;
            }
            var patos = prevState.patos.slice();
            let numeroPatos = prevState.numeroDePatos + 1;
            let side = this.getRandomSide();
            let idPato = numeroPatos;
            patos.push({
                id: (idPato <= 9999 ? ("000" + idPato).slice(-4) : idPato) + '.',
                position: this.getRandomElement(this.espaciosDisponibles, prevState.lastLine),
                velocidad: this.state.velocidadDefault,
                side: side
            });

            console.log(numeroPatos);
            return {
                patos: patos,
                numeroDePatos: numeroPatos
            }
        });
    }

    logout(){
        AuthService.logout();
        this.props.history.push('/');
    }

    getRandomElement(array, lastSide, last) {
        let randomPos = null;
        do {
            randomPos = Math.floor(Math.random() * array.length);
        } while (randomPos === lastSide);

        return array[randomPos];
    }

    getRandomSide() {
        let randomPos = Math.floor(Math.random() * 10);
        return randomPos > 5 ? 'left' : 'right';
    }



    render() {
        return (

            <div class="game-scene" >
                
                <nav className="nav-game">
                    <div class="nav-wrapper">
                        <RoundButton onClick={this.start} name="START" disabled={this.state.paused || this.state.started} />
                        <a href="" class="brand-logo center"> {AuthService.user().nombre}: {this.state.disparosEnPatos}</a>
                        <ul id="nav-mobile" class="right">
                            <RoundButton name="PAUSE" onClick={this.pause} disabled={!this.state.started} />
                            <RoundButton name="STOP" onClick={this.stop} disabled={!this.state.started} />
                            <RoundButton name="OUT" onClick={this.logout}  />
                        </ul>
                    </div>
                </nav>
                {this.state.paused ? <Pausa /> : null}
                <div className="App-game">
                    {this.state.patos.map((item, i) =>
                        <Pato key={item.id}
                            position={item.position}
                            id={item.id}
                            onFueraRango={this.onLost}
                            pause={this.state.paused}
                            side={item.side}
                            onRecibirDisparo={() => this.aumentarContador(item.id)}
                            ref={(pato) => { this.patos[item.id] = pato; }}
                        />)}
                </div>

            </div>
        );
    }
}

export default Juego;