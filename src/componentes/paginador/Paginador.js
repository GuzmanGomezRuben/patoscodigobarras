import React, { Component } from 'react';

class Paginador extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            elements: [...this.getArray(props.size, props.sizePage)],
            sizePage: props.sizePage || 3,
            position: 0
        };
    }

    componentWillReceiveProps(props) {
        this.setState({
            elements: [...this.getArray(props.size, props.sizePage)]
        });
    }

    getArray(size, sizePage) {
        let pageNumbers = Math.round(((size / (sizePage || 3)) ));
        console.log('Page numbers: ', pageNumbers);
        return Array(pageNumbers).map((el, i) => el);
    }

    onChangePage(position){
        if(this.state.position === position){
            return;
        }

        this.setState({
            position: position
        });
        
        if(this.props.onChangePage){
            this.props.onChangePage(position);
        }
    }
    


    render() {
        return (
            <ul class="pagination">
                <li class="disabled"><a><i class="material-icons">chevron_left</i></a></li>
                {this.state.elements.map((el, i) => <li onClick={() => this.onChangePage(i)} key={i} className={i === this.state.position ? 'active orange accent-4' : ' waves-effect'}><a>{i + 1}</a></li>)}
                <li class="waves-effect"><a><i class="material-icons">chevron_right</i></a></li>
            </ul>
        )
    }




}

export default Paginador;