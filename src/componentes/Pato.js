import React, { Component } from 'react';
import Barcode from 'react-barcode';
import 'create-react-class';
import './Pato.css';

class Pato extends Component {

    constructor(props) {
        super(props);
        this.size = 100;
        this.state = {
            styles: this.getInitialState.bind(this)(this.props.side),
            patoStyle: {
            }
        };
        this.onRecibirDisparo = this.props.onRecibirDisparo;
        this.logoPato = (this.props.side === 'left') ? 'left' : 'right';
        this.pato = (<div id={this.props.id} className={'pato ' + this.logoPato} style={this.state.patoStyle} onClick={this.onRecibirDisparo} >
            <Barcode value={this.props.id} className='barcode'
                displayValue={false} width={2} height={30}
                background={'#ffff00'}
                margin={0}
            /></div>);
    }



    componentDidMount() {
        console.log("Pato montado", this.props.id);
    }

    getInitialState(side) {
        if (side === 'left') {
            return this.getStyleLeft();
        }

        return this.getStyleRight();

    }

    _getDefaultConfig() {
        return {
            top: ((this.props.position) * this.size),
            height: this.size,
            width: this.size,
        };
    }

    getStyleLeft() {
        let left = {
            left: 1
        };

        return Object.assign(this._getDefaultConfig(), left);
    }

    getStyleRight() {
        let right = {
            right: 1
        };

        return Object.assign(this._getDefaultConfig(), right);
    }

    getStyles() {
        return this.state.styles;
    }

    setStyles() {
        if (this.props.pause) {
            return;
        }

        let patoElement = document.getElementById(this.props.id);
        let bounds = patoElement && patoElement.getBoundingClientRect();
        this.verificaFueraRango(bounds);
        let newBounds = this.getNextPos(bounds);
        this.setState(newBounds);

    }

    verificaFueraRango(bounds) {
        let windowBounds = this._getSizeWindow();
        if (this.props.onFueraRango) {
            if ((bounds.left + bounds.width) < 0 || (bounds.left) > (windowBounds.width)) {
                this.props.onFueraRango();
            }
        }
    }

    getNextPos(bounds) {
        if (this.props.side === 'left') {
            return this._getNextLeft(bounds);
        }
        return this._getNextRight(bounds);
    }

    render() {
        return (
            <div class="animal-container" style={this.state.styles}>
                {this.pato}
            </div>
        )
    }

    _getSizeWindow() {
        return document.body.getBoundingClientRect();
    }

    _getNextRight(duckBounds, windowBounds) {
        var styles = this.getStyles();
        return {
            styles: {
                right: styles.right + 1,
                top: styles.top,
                width: styles.width,
                height: styles.height
            },
        }
    }

    _getNextLeft(duckBounds, windowBounds) {
        var styles = this.getStyles();
        return {
            styles: {
                left: styles.left + 1,
                top: styles.top,
                width: styles.width,
                height: styles.height
            },
        }
    }


}

export default Pato;