
import React, { Component } from 'react';
import './Tabla.css';
import Paginador from '../paginador/Paginador';

class Tabla extends Component {



    constructor(props) {
        super(props);
        let pageSize = this.props.pageSize || 3;
        this.backupData = [].concat(this.props.body || []);
        this.inputsEl = [];
        this.state = {
            headers: this.props.headers || [],
            body: this.getTableChunk(this.props.body || [], 0, pageSize),
            order: true,
            inputsHeader: [],
            pageSize: pageSize,
            isFilter: false,
            isOrder: false,
            position: 0
        };

        this.orderBy = this.orderBy.bind(this);
        this.parseHeader = this.parseHeader.bind(this);
        this.filter = this.filter.bind(this);
        this.updateTable = this.updateTable.bind(this);

    }

    parseHeader(header, index) {
        return (<th key={index} >
            {header.title}
            {header.order !== false ? <i className="material-icons tiny" onClick={() => this.orderBy(header)}>unfold_more</i> : null}
            {header.filter !== false ?
                <div className="input-field search">
                    <input id="first_name" type="text" className="validate"
                        ref={(el) => this.inputsEl[index] = el}
                        onChange={this.handleFilter(index)} />
                </div> : null
            }

        </th>);
    }



    parseBody(fila, index) {
        return (
            <tr key={index}>
                {this.props.headers.map((header, index) => (<td key={index}>{fila[header.key]}</td>))}
            </tr>
        );
    }

    handleFilter = (index) => (event) => {
        this.setState((prevState) => {          
            return {
                inputsHeader : this.inputsEl.map((el) => el.value)
            }
        })
        this.filter();
    }

    filter() {
        if (this.filterTimeout) {
            window.clearTimeout(this.filterTimeout);
        }

        this.filterTimeout = window.setTimeout(() => {
            this.setState((prevState) => {
                this.backupDataFilter = this.backupData.filter(this.getFilter(prevState.headers, prevState.inputsHeader));
                let isFilter = this.backupDataFilter.length !== this.backupData.length;

                return {
                    body: this.getTableChunk(this.backupDataFilter, 0, prevState.pageSize),
                    isFilter: isFilter
                }
            });
        }, 1000);
    }

    getFilter = (headers, inputsHeader) => (row) => {
        return headers.every((header, index) => {
            let contentInput = (row[header.key] || "").toString();
            if (!inputsHeader[index]) {
                return true;
            }
            return contentInput.toString().includes(inputsHeader[index])
        })
    }

    orderBy(header) {

        this.setState((prevState) => {
            this.backupDataOrdered = [].concat(prevState.isFilter ? this.backupDataFilter : this.backupData).sort(this.getOrder(header, prevState.order));
            return {
                order: !prevState.order,
                body: this.getTableChunk(this.backupDataOrdered, 0, prevState.pageSize),
                isOrder: true
            }
        })
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (this.state.body !== this.backupData) {
            return true;
        }

        if (this.state.header !== nextState.headers) {
            return true;
        }

        return false;
    }

    getOrder(header, order) {
        if (header.orderBy) {
            return header.orderBy();
        }
        if (order) {
            return (rowA, rowB) => (typeof rowA[header.key] === 'string' ? rowA[header.key].localeCompare(rowB[header.key]) : rowA[header.key] > rowB[header.key]);
        }

        return (rowA, rowB) => (typeof rowB[header.key] === 'string' ? rowB[header.key].localeCompare(rowA[header.key]) : rowA[header.key] < rowB[header.key]);

    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.body === this.props.body && nextProps.headers === this.props.headers){
            return;
        }

        this.backupData = [].concat(nextProps.body || []);
        let pageSize = this.props.pageSize || 3;
        this.setState({
            headers: nextProps.headers || [],
            body: this.getTableChunk(nextProps.body || [], 0, pageSize),
            pageSize: pageSize
        });

    }

    updateTable(pageNumber) {
        this.setState((prevState) => ({
            body: this.getTableChunk(prevState.isFilter ? this.backupDataFilter : (prevState.isOrder ? this.backupDataOrdered : this.backupData), pageNumber, prevState.pageSize),
            position: pageNumber
        }));
    }

    getTableChunk(tableBody, pageNumber, pageSize) {
        let indice = pageNumber * pageSize;
        console.log(tableBody, pageNumber);
        return tableBody.slice(indice, indice + pageSize)
    }

    render() {
        return (
            <div>
                <table className="table highlight">
                    <thead>
                        <tr>
                            {this.state.headers.map(this.parseHeader)}
                        </tr>
                    </thead>

                    <tbody>
                        {this.state.body.map(this.parseBody.bind(this))}
                    </tbody>
                </table>
                <div className="center-align">
                    <Paginador size={this.state.isFilter ? this.backupDataFilter.length : this.backupData.length} onChangePage={this.updateTable} />
                </div>
            </div>

        )
    }

}

export default Tabla;