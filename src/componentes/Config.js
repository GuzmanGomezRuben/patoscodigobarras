const url = 'http://localhost:2000';
const Config = {
    URL: url,
    endpoints: {
        players:{
            all: url + '/recents',
            register: url + '/players',
            update: url + '/players'
        }
    }
};

export default Config;