import React, { Component } from 'react';
import './Registro.css';
import Tabla from '../tabla/Tabla';
import Config from '../Config';
import AuthService from '../../services/AuthService';
import User from '../../services/UserService';

class Registro extends Component {

    constructor(props) {
        super(props);
        this.registrar = this.registrar.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.state = {
            nombre: '',
            headers: [{ title: 'Nombre', key: 'nombre' }, { title: 'Puntos', key: 'puntos' }],
            body: []
        };
    }

    componentDidMount() {
        this.getPlayers();
    }

    getPlayers() {
        return User.all().then((response) => {
            this.setState({
                body: response.data.data.map((row) => {
                    row.continuar = <a className="waves-effect waves-light btn orange accent-4" onClick={this.continuar(row)}>Continuar</a>
                    return row;
                })
            });
        });
    }

    continuar = (row) => (e) => {
        alert("Continua: " + row.nombre);
    }

    registrar(event) {
        let player = this.state.body.find(player => this.state.nombre === player.nombre);
        if (player) {
            return this._redirectToGame(player);
        }

        User.register({
            name: this.state.nombre
        }).then(() => this._redirectToGame({
            nombre: this.state.nombre,
            puntos: 0
        })).catch((error) => {
            alert(error.response.data.error);
        })

        event.preventDefault();
    }

    _redirectToGame(user) {
        AuthService.login(user);
        this.setState({ nombre: '' });        
        this.props.history.push('/game')
    }

    handleChange(event) {
        this.setState({ nombre: event.target.value });
    }

    render() {
        return (
            <div id="background" className="orange darken-1">
                <div id="form" className="container">
                    <div className="row">
                        <div className="col s12">
                            <div className="card-panel">
                                <h3 className="center" >¡Bienvenido!</h3>
                                <div class="row">
                                    <form class="col s12" onSubmit={this.registrar} >
                                        <div class="row">
                                            <div class="input-field col s12 text-orange">
                                                <input
                                                    id="first_name" type="text"
                                                    value={this.state.nombre}
                                                    className="validate"
                                                    required
                                                    onChange={this.handleChange}
                                                />
                                                <label for="first_name">Tu nombre</label>

                                            </div>
                                        </div>
                                    </form>
                                </div>
                                {this.state.body.length ? <Tabla headers={this.state.headers} body={this.state.body} /> : null}

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default Registro;