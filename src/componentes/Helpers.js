const Helpers = {
    formatUrl: function (url, paramenters) {
        for (let param in paramenters) {
            if (paramenters.hasOwnProperty(param)) {
                url = url.replace(':' + param, paramenters[param]);
            }
        }
        return url;
    }
};

export default Helpers;